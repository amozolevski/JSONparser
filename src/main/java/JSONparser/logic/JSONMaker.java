package JSONparser.logic;

import org.json.JSONObject;

import java.util.List;

public class JSONMaker {

    private String model;
    private String modelType;
    private List<String> coffeeTypeList;
    private int pressureLevel;
    private List<String> functionsList;
    private double waterCapacity;
    private String color;
    private int power;
    private double weight;
    private String manufacturer;

    public JSONObject makeJSON(){

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("Model", this.getModel());
        jsonObject.put("ModelType", this.getModelType());
        jsonObject.put("CoffeeType", this.getCoffeeTypeList());
        jsonObject.put("PressureLevel", this.getPressureLevel());
        jsonObject.put("Functions", this.getFunctionsList());
        jsonObject.put("WaterCapacity", this.getWaterCapacity());
        jsonObject.put("Color", this.getColor());
        jsonObject.put("Power", this.getPower());
        jsonObject.put("Weight", this.getWeight());
        jsonObject.put("Manufacturer", this.getManufacturer());

        return jsonObject;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public void setCoffeeTypeList(List<String> coffeeTypeList) {
        this.coffeeTypeList = coffeeTypeList;
    }

    public void setPressureLevel(int pressureLevel) {
        this.pressureLevel = pressureLevel;
    }

    public void setFunctionsList(List<String> functionsList) {
        this.functionsList = functionsList;
    }

    public void setWaterCapacity(double waterCapacity) {
        this.waterCapacity = waterCapacity;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getModelType() {
        return modelType;
    }

    public List<String> getCoffeeTypeList() {
        return coffeeTypeList;
    }

    public int getPressureLevel() {
        return pressureLevel;
    }

    public List<String> getFunctionsList() {
        return functionsList;
    }

    public double getWaterCapacity() {
        return waterCapacity;
    }

    public String getColor() {
        return color;
    }

    public int getPower() {
        return power;
    }

    public double getWeight() {
        return weight;
    }

    public String getManufacturer() {
        return manufacturer;
    }
}