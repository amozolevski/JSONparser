package JSONparser.logic;

import java.util.List;

public class CoffeeMachine {

    private String model;
    private String modelType;
    private List<String> coffeeTypeList;
    private int pressureLevel;
    private List<String> functionsList;
    private double waterCapacity;
    private String color;
    private int power;
    private double weight;
    private String manufacturer;

    public CoffeeMachine(String model, String modelType, List<String> coffeeTypeList, int pressureLevel,
                         List<String> functionsList, double waterCapacity, String color, int power,
                         double weight, String manufacturer) {

        this.model = model;
        this.modelType = modelType;
        this.coffeeTypeList = coffeeTypeList;
        this.pressureLevel = pressureLevel;
        this.functionsList = functionsList;
        this.waterCapacity = waterCapacity;
        this.color = color;
        this.power = power;
        this.weight = weight;
        this.manufacturer = manufacturer;
    }

    private String getStringFromList(List<String> list){
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < list.size(); i++) {
            if(i == list.size() - 1)
                stringBuilder.append(list.get(i));
            else
                stringBuilder.append(list.get(i)).append(", ");
        }

        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return "CoffeeMachine: \n" +
                "Model = \"" + model + "\"\n" +
                "Model Type = \"" + modelType + "\"\n" +
                "Coffee Type = \"" + getStringFromList(coffeeTypeList) + "\"\n" +
                "Pressure Level = \"" + pressureLevel + "\"\n" +
                "Functions = \"" + getStringFromList(functionsList) + "\"\n" +
                "Water Capacity = \"" + waterCapacity + "\"\n" +
                "Color = \"" + color + "\"\n" +
                "Power = \"" + power + "\"\n" +
                "Weight = \"" + weight + "\"\n" +
                "Manufacturer = \"" + manufacturer + "\"" +
                "\n" ;
    }
}
