package JSONparser.logic;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class XMLStaxParser {

    public static List<JSONMaker> parseXML(String filePath){

        List<JSONMaker> coffeeMachinesList = new ArrayList<>();
        JSONMaker jsonMaker = null;
        List<String> coffeeTypesList = null;
        List<String> functionsList = null;
        XMLInputFactory factory = XMLInputFactory.newInstance();

        try {
            XMLEventReader xmlEventReader = factory.createXMLEventReader(new FileReader(filePath));

            while (xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();

                if(xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();

                    if(startElement.getName().getLocalPart().equals("CoffeeMachine")){
                        jsonMaker = new JSONMaker();
                    }

                    if(startElement.getName().getLocalPart().equals("Model")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setModel(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("ModelType")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setModelType(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("CoffeeTypes")) {
                        coffeeTypesList = new ArrayList<>();
                    }

                    if(startElement.getName().getLocalPart().equals("CoffeeType")){
                        xmlEvent = xmlEventReader.nextEvent();
                        coffeeTypesList.add(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("PressureLevel")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setPressureLevel(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equals("Functions")){
                        functionsList = new ArrayList<>();
                    }

                    if(startElement.getName().getLocalPart().equals("Function")){
                        xmlEvent = xmlEventReader.nextEvent();
                        functionsList.add(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("WaterCapacity")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setWaterCapacity(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equals("Color")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setColor(xmlEvent.asCharacters().getData());
                    }

                    if(startElement.getName().getLocalPart().equals("Power")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setPower(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equals("Weight")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setWeight(Double.parseDouble(xmlEvent.asCharacters().getData()));
                    }

                    if(startElement.getName().getLocalPart().equals("Manufacturer")){
                        xmlEvent = xmlEventReader.nextEvent();
                        jsonMaker.setManufacturer(xmlEvent.asCharacters().getData());
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equals("CoffeeTypes")){
                        jsonMaker.setCoffeeTypeList(coffeeTypesList);
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equals("Functions")){
                        jsonMaker.setFunctionsList(functionsList);
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();

                    if(endElement.getName().getLocalPart().equals("CoffeeMachine")){
                        coffeeMachinesList.add(jsonMaker);
                    }
                }
            }
        } catch (XMLStreamException xmlse) {
            System.err.println("Error: " + xmlse.getMessage());
        } catch (FileNotFoundException fnfe) {
            System.err.println("File did not found " + fnfe.getMessage());
        }

        return coffeeMachinesList;
    }
}