package JSONparser.logic;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JSONParser {

    public static CoffeeMachine parseJSON(String responce){

        JSONObject jsonObject = new JSONObject(responce);
        JSONArray jsonCoffeeTypeList = jsonObject.getJSONArray("CoffeeType");
        JSONArray jsonFunctionsList = jsonObject.getJSONArray("Functions");

        List<String> coffeeTypeList = new ArrayList<>();
        List<String> functionsList = new ArrayList<>();

        for (Object obj : jsonCoffeeTypeList) {
            coffeeTypeList.add(obj.toString());
        }

        for (Object obj : jsonFunctionsList) {
            functionsList.add(obj.toString());
        }

        return new CoffeeMachine(jsonObject.getString("Model"), jsonObject.getString("ModelType"),
                                coffeeTypeList, jsonObject.getInt("PressureLevel"), functionsList,
                                jsonObject.getDouble("WaterCapacity"), jsonObject.getString("Color"),
                                jsonObject.getInt("Power"), jsonObject.getDouble("Weight"), jsonObject.getString("Manufacturer"));

    }
}