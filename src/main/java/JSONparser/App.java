package JSONparser;

import JSONparser.logic.CoffeeMachine;
import JSONparser.logic.JSONMaker;
import JSONparser.logic.JSONParser;
import JSONparser.logic.XMLStaxParser;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        String fileName = "src/main/java/JSONparser/XMLdata/CoffeeMachines.xml";

        List<JSONMaker> jsonObjects = XMLStaxParser.parseXML(fileName);
        List<CoffeeMachine> coffeemats = new ArrayList<>();

        for (JSONMaker j : jsonObjects) {
            JSONObject object = j.makeJSON();
            System.out.print("JSON : ");
            System.out.println(object);
            System.out.println();
            coffeemats.add(JSONParser.parseJSON(object.toString()));
        }

        for (CoffeeMachine c : coffeemats) {
            System.out.println(c);
        }
    }
}